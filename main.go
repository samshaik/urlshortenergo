package main

import (
	"flag"
	"fmt"
	"github.com/samshaik25/urlshortener/urlshort"
	"log"
	"net/http"
)

func main() {
	fileName := flag.String("file", "redirect.json", "file name with redirection URLs")
	flag.Parse()

	mux := defaultMux()
	startServer(mux, fileName)
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux
}

func startServer(mux *http.ServeMux, fileName *string) {
	UrlMapper, err := urlshort.UrlMapper(*fileName)

	if err != nil {
		log.Fatalf("Can't create redirect URL provider. %v", err)
	}

	yamlHandler := urlshort.NewHttpRedirectHandler(UrlMapper)

	fmt.Println("Starting the server on :8080")
	if err := http.ListenAndServe(":8080", yamlHandler); err != nil {
		log.Fatalf("Server error: %v", err)
	}
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}
