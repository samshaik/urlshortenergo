package urlshort

import (
	"encoding/json"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

/*
* reads a configuration file (JSON or YAML) and returns a map of path-to-URL mappings.
*
* @param filename: The name of the configuration file.
* @return A URL map and an error, if any.
 */
func UrlMapper(filename string) (map[string]string, error) {
	content, err := ioutil.ReadFile(filename)

	if err != nil {
		return nil, err
	}
	data := string(content)
	jsn := []map[string]string{}
	if strings.HasPrefix(data, "{") || strings.HasPrefix(data, "[") {
		log.Println("Unmarshalling JSON file.")
		err = json.Unmarshal(content, &jsn)
	} else {
		log.Println("Unmarshalling YAML file.")
		err = yaml.Unmarshal(content, &jsn)
	}

	if err != nil {
		return nil, err
	}

	mapping := make(map[string]string)

	for _, m := range jsn {
		mapping[m["path"]] = m["url"]
	}

	return mapping, nil
}

/*
* creates an HTTP handler for URL redirection based on a map of path-to-URL mappings.
*
* @param mapper: A URL mapping.
* @return An HTTP handler function for URL redirection.
 */
func NewHttpRedirectHandler(mapper map[string]string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if url, ok := mapper[r.URL.Path]; ok {
			log.Printf("Redirecting %s to %s\n", r.URL.Path, url)
			http.Redirect(w, r, url, http.StatusFound)
		} else {
			http.Error(w, "Invalid path", http.StatusNotFound)
		}
	}
}
