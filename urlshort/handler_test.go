package urlshort

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
)

func TestUrlMapperJsonFile(t *testing.T) {
	// Create a temporary JSON file with test data
	tmpFileName := "testdata/test.json"
	content := []byte(`[
		{"path": "/path1", "url": "https://example.com/url1"},
		{"path": "/path2", "url": "https://example.com/url2"}
	]`)
	if err := ioutil.WriteFile(tmpFileName, content, 0644); err != nil {
		t.Fatalf("Error creating test file: %v", err)
	}
	defer os.Remove(tmpFileName)

	// Test UrlMapper with the temporary test JSON file
	mapping, err := UrlMapper(tmpFileName)
	if err != nil {
		t.Fatalf("UrlMapper error: %v", err)
	}

	expected := map[string]string{
		"/path1": "https://example.com/url1",
		"/path2": "https://example.com/url2",
	}

	// Compare the actual mapping with the expected mapping
	if !reflect.DeepEqual(mapping, expected) {
		t.Errorf("UrlMapper result mismatch.\nExpected: %v\nActual: %v", expected, mapping)
	}
}

func TestUrlMapperWithYAML(t *testing.T) {
	// Create a temporary YAML file with test data
	tmpYAMLFileName := "testdata/test.yaml"
	yamlContent := []byte(`
- path: "/yaml1"
  url: "https://example.com/yaml1"
- path: "/yaml2"
  url: "https://example.com/yaml2"
`)
	if err := ioutil.WriteFile(tmpYAMLFileName, yamlContent, 0644); err != nil {
		t.Fatalf("Error creating test YAML file: %v", err)
	}
	defer os.Remove(tmpYAMLFileName)

	mapping, err := UrlMapper(tmpYAMLFileName)
	if err != nil {
		t.Fatalf("UrlMapper error: %v", err)
	}

	expected := map[string]string{
		"/yaml1": "https://example.com/yaml1",
		"/yaml2": "https://example.com/yaml2",
	}

	// Compare the actual mapping with the expected mapping
	if !reflect.DeepEqual(mapping, expected) {
		t.Errorf("UrlMapper result mismatch.\nExpected: %v\nActual: %v", expected, mapping)
	}
}


func TestNewHttpRedirectHandler(t *testing.T) {
	mapping := map[string]string{
		"/test1": "https://example.com/test1",
		"/test2": "https://example.com/test2",
	}

	// Create an HTTP request with a path
	req := httptest.NewRequest("GET", "/test1", nil)

	// Create an HTTP response recorder
	w := httptest.NewRecorder()

	handler := NewHttpRedirectHandler(mapping)

	// Serve the HTTP request with the handler
	handler(w, req)

	if w.Code != http.StatusFound {
		t.Errorf("Expected status code %d, got %d", http.StatusFound, w.Code)
	}

	// Check the Location header in the response
	expectedLocation := "https://example.com/test1"
	if location := w.Header().Get("Location"); location != expectedLocation {
		t.Errorf("Expected Location header %s, got %s", expectedLocation, location)
	}
}