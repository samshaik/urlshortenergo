# Go URL Shortener

A simple Go URL shortener for creating custom redirects from paths to URLs.

## Features

- Create custom redirects using JSON or YAML configuration files.
- Run an HTTP server for easy testing and deployment.

## Usage

1. **Prerequisites**: Install Go on your system from [here](https://golang.org/dl/).

2. **Installation**:
   ```
   git clone git@gitlab.com:samshaik/urlshortenergo.git
   cd url-shortener
   go build

3. **Configuration**:
 
    Create a Configuration File:
    Create a JSON or YAML file with path-to-URL mappings.
    Example YAML:
    yaml
    ```
    - path: /example
      url: https://example.com 
    ```

    Example Json file 
    ```
    [
    {
        "path": "/example",
        "url": "https://example.com"
    }
    ]
    ```

4. **Start the Server**:
    ```
    go run main.go
    ```
    default file will redirect.json
    or
     ```
    ./urlshortener -file redirect.yaml
    ```
    Replace redirect.yaml with your configuration file.


    
    Access Custom Redirects:
    Visit http://localhost:8080/example to redirect to https://example.com.

5. **To run test**
   ```
   go test ./...
   ```
